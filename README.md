Minimal boilerplate to kickstart React projects with Typescript.

Builds with webpack, development with webpack-dev-server.

Tests with jest and enzyme.